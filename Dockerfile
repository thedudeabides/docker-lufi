FROM alpine:3.8

ARG LUFI_VERSION=0.03.5

ENV GID=9991 \
    UID=9991 \
    SECRET=0423bab3aea2d87d5eedd9a4e8173618 \
    CONTACT=contact@example.com \
    MAX_FILE_SIZE=100000000 \
    WEBROOT=/ \
    DEFAULT_DELAY=1 \
    MAX_DELAY=0 \
    THEME=default \
    ALLOW_PWD_ON_FILES=1 \
    POLICY_WHEN_FULL=warn \
    REPORT=report@example.com

RUN set -ex; \
      apk add --update --no-cache --virtual .build-deps \
                build-base \
                libressl-dev \
                ca-certificates \
                tar \
                perl-dev \
                libidn-dev \
                wget \
    && apk add --update --no-cache \
                libressl \
                perl \
                libidn \
                perl-crypt-rijndael \
                perl-test-manifest \
                perl-net-ssleay \
                tini \
                su-exec \
    && echo | cpan \
    && cpan install Carton \
    && wget https://framagit.org/fiat-tux/hat-softwares/lufi/-/archive/${LUFI_VERSION}/lufi-${LUFI_VERSION}.tar.bz2 \
    && tar xvjf lufi-${LUFI_VERSION}.tar.bz2 \
    && mv lufi-${LUFI_VERSION} /usr/lufi \
    && rm lufi-${LUFI_VERSION}.tar.bz2 \
    && cd /usr/lufi \
    && echo "requires 'Crypt::Rijndael', url => 'https://gitlab.com/thedudeabides/crypt-rijndael/-/archive/musl-libc/crypt-rijndael-musl-libc.tar.gz';" >> cpanfile \
    && rm cpanfile.snapshot \
    && carton install --without=test --without=postgresql --without=mysql --without=ldap \
    && apk del .build-deps \
    && rm -rf /var/cache/apk/* /root/.cpan* /usr/lufi/local/cache/*

VOLUME /usr/lufi/data /usr/lufi/files

EXPOSE 5000

COPY startup /usr/local/bin/startup
COPY lufi.conf /usr/lufi/lufi.conf
COPY lufi_cron /etc/periodic/hourly/lufi

CMD ["/usr/local/bin/startup"]
